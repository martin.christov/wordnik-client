const request = require('superagent')
const { api_key, base } = require('../../api-config')

const pingWordnik = () => request
   .get(`${base}/words.json/randomWord`)
   .timeout({
      response: 5000,
      deadline: 60000
   })
   .query({
      api_key
   })

const check = (req, res) => pingWordnik()
   .then(({ body }) => res.json(body))
   .catch(err => res.status(err.timeout ? 408 : 500).json(err))

module.exports = {
   check
}
